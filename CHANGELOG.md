# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.0-dev.2](https://gitlab.com/igbmc/it/madbot-client/compare/v1.0.0-dev.1...v1.0.0-dev.2) (2023-11-05)


### Bug Fixes

* add missing entrypoint ([7dbad16](https://gitlab.com/igbmc/it/madbot-client/commit/7dbad168316ca5554dcfac2f148de7a4badf028d))

## 1.0.0-dev.1 (2023-11-05)


### Features

* "metadata" added to the text in reference to metark ([f69879b](https://gitlab.com/igbmc/it/madbot-client/commit/f69879b4db16eb8a5898f7936caf4d797541b630))
* activate auto release ([e10bcf2](https://gitlab.com/igbmc/it/madbot-client/commit/e10bcf25d0991d3d4b584fa2cf5988fc8f955c4c))
* adapt error display to conventional error messages ([92a88ca](https://gitlab.com/igbmc/it/madbot-client/commit/92a88ca4c3b775ef582c7f0435816b973a9408cd))
* adapt the creation of multiple members ([e6c69f2](https://gitlab.com/igbmc/it/madbot-client/commit/e6c69f26c04dde7aadbafa77376f799f8f876ae7))
* Add button to generate demo ([e1fe553](https://gitlab.com/igbmc/it/madbot-client/commit/e1fe553dd98cac3df1ebca4934a1906bbc7ebd50))
* add keyup enter to study and assay delete ([274c765](https://gitlab.com/igbmc/it/madbot-client/commit/274c7654a74a8a896f2869c6ce037a46f2bf48d3))
* add two template for the chips ([f2a02cf](https://gitlab.com/igbmc/it/madbot-client/commit/f2a02cfedb689d5e226364639d954d0c1cef1df4))
* click on username to add token to clipboard ([50583c3](https://gitlab.com/igbmc/it/madbot-client/commit/50583c322f91e18056cfb90d5046083468b333ed))
* create CI config file ([37018af](https://gitlab.com/igbmc/it/madbot-client/commit/37018af9305d1c3be53ad4d937e2a55ee3e702e1))
* create release config file ([3c89f62](https://gitlab.com/igbmc/it/madbot-client/commit/3c89f62ae75e74960604315de61e9b963e3ce33d))
* diseable change member role for me ([c33ca52](https://gitlab.com/igbmc/it/madbot-client/commit/c33ca52dbdb047856ef7bea33d1a63ffecc6f3f8))
* display dataobjects descriptions and links when creating a new datalink ([8d43646](https://gitlab.com/igbmc/it/madbot-client/commit/8d4364605181b3375186e6dc183ecb21d3e4d4a2))
* handle errors in members ([d7fc71a](https://gitlab.com/igbmc/it/madbot-client/commit/d7fc71a3194a71e8c2116b02fe3d5d1018915c94))
* handle members with the same role in chips ([91664ce](https://gitlab.com/igbmc/it/madbot-client/commit/91664ce5657f89be509c3ab235d43f31f6bbfaf4))
* improve my member management in investigation ([922ac41](https://gitlab.com/igbmc/it/madbot-client/commit/922ac41934ae8ee44a45ddac9bab64a40987d94b))
* include investigation id when creating a new tool ([b05f133](https://gitlab.com/igbmc/it/madbot-client/commit/b05f13374f6d6637851d34017e953e0007b02216))
* keyup enter with latest vuetify version ([a8723c5](https://gitlab.com/igbmc/it/madbot-client/commit/a8723c5ccf8de8ad3448741fb3f7688a70b88cbb))
* license changed to BS3 ([e6f074b](https://gitlab.com/igbmc/it/madbot-client/commit/e6f074b2cd95ff95f3bd7142af14283794a5dc35))
* manage API error ([dfb63db](https://gitlab.com/igbmc/it/madbot-client/commit/dfb63db3c1420e5df1d3def8952b627994c9f575))
* manage investigation members ([c401d06](https://gitlab.com/igbmc/it/madbot-client/commit/c401d06312ea9859f6e89d6c155df86cdd97131c))
* name adapted to madbot for the ts files ([04839b7](https://gitlab.com/igbmc/it/madbot-client/commit/04839b75ca15445a03b54b9145c03946603617bb))
* openlink renamed to madbot on client side ([c2f0dad](https://gitlab.com/igbmc/it/madbot-client/commit/c2f0dad17c69a33d2710ade05b95d81eabc25567))
* prerelease ([5af3396](https://gitlab.com/igbmc/it/madbot-client/commit/5af33968b834428574f2b4bf90c14580a1a811ce))
* refacfor connectors, tools and dataobject icon display through URLs or Font Awesome icons ([640f0a6](https://gitlab.com/igbmc/it/madbot-client/commit/640f0a63fd4c2cb1652512832a6c567fcfcf1ac8))
* tool requests sent with the investigation id ([dedee34](https://gitlab.com/igbmc/it/madbot-client/commit/dedee345ee65ad48e06c3a0184a2758dd96c0ca9))


### Bug Fixes

* add autofocus to assay deletion ([4342ac7](https://gitlab.com/igbmc/it/madbot-client/commit/4342ac7bada40d94a316bc2f72328274160a14dc))
* delete the package-lock file ([128b030](https://gitlab.com/igbmc/it/madbot-client/commit/128b030dce91ecfbb55338aa8cb70042005ae914))
* display selected dataobject info ([d45e4aa](https://gitlab.com/igbmc/it/madbot-client/commit/d45e4aae399bd306b2f862018b02c5658c5c10de))
* ensure .env variables are available in production ([9cbc505](https://gitlab.com/igbmc/it/madbot-client/commit/9cbc5052bbbada9f9e0f30e0eb46e415b7d856ec))
* handle error in members ([55d7506](https://gitlab.com/igbmc/it/madbot-client/commit/55d7506806cc7344f31eba609f35752ed18428e4))
* remove tooltip  when the token is copied ([212aa7f](https://gitlab.com/igbmc/it/madbot-client/commit/212aa7f7dc58c10901ab73b289e7fa4cbaa9a0e4))
* remove unused props ([adc0e6c](https://gitlab.com/igbmc/it/madbot-client/commit/adc0e6cfab4248a035f04ad6caf72b72fc856e48))
* resolve dataobject infinte loading error ([b9ef1b4](https://gitlab.com/igbmc/it/madbot-client/commit/b9ef1b4949248d263468877a6c15629c5e29faf9))
