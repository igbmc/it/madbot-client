// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    runtimeConfig: {
        secret: process.env.NUXT_SECRET,
        oidcWellKnown: process.env.NUXT_OIDC_WELL_KNOWN,
        oidcIssuer: process.env.NUXT_OIDC_ISSUER,
        oidcClientID: process.env.NUXT_OIDC_CLIENT_ID,
        oidcClientSecret: process.env.NUXT_OIDC_CLIENT_SECRET,
        public: {
          madbotApiURL: process.env.NUXT_PUBLIC_MADBOT_API_URL,
        },
        authentication: process.env.NUXT_AUTHENTICATION,
    },
    css: [
        "vuetify/lib/styles/main.sass",
        "@mdi/font/css/materialdesignicons.min.css",
        '@fortawesome/fontawesome-free/css/all.css',
        "@/assets/css/madbot.css"
    ],
    build: {
        transpile: ["vuetify"],
    },
    vite: {
        define: {
        "process.env.DEBUG": false,
        },
    },
    modules: [
        "@sidebase/nuxt-auth",
        '@pinia/nuxt',
    ],
    auth: {
        enableGlobalAppMiddleware: true,
        origin: process.env.ORIGIN,
    },
    routeRules: {
        '/todo': { ssr: false},
        '/index': { ssr: false },
        '/investigations/*': { ssr: false},
        '/investigations/*/studies/*': { ssr: false},
        '/investigations/*/studies/*/assays/*': { ssr: false},
    }
});
